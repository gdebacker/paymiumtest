import Vue from 'vue';
import Router from 'vue-router';
import 'material-design-icons-iconfont/dist/material-design-icons.css';
import Transactions from '@/components/Transactions';
import Home from '@/components/Home';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/transactions',
      name: 'transactions',
      component: Transactions,
    },
  ],
});
